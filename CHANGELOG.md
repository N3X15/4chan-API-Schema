A list of major changes to the API, and to the repo.

# v2018.10.02

## Repository

* Major reorganization so it looks like I at least tried to make things look decent.
* Added OpenAPI v3.0.0 docs.
* Updated OpenAPI v2.0 docs.
* Added a script that keeps everything in sync. (BUILD.py)