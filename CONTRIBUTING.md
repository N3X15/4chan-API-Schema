# Contributions

Pull Requests are welcome, provided they are not terrible and only address inaccuracies.  The purpose of this repository is merely to house the schema themselves.

## Rules

* Programmatic APIs do not belong in this repository.  Do not commit them.  The only scripts should be there to check the validity of the schemas themselves, for CI purposes.
* Indent with two spaces.
  * `.editorconfig` and `.jsbeautifyrc` contain settings for compatible editors/style utilities.
* Do not break line endings.  [Read this for more](https://help.github.com/articles/dealing-with-line-endings/).
