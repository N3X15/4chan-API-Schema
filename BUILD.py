import os, sys, shutil
from buildtools import log, cmd, os_utils

from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.convert_data import ConvertDataBuildTarget, EDataType
from buildtools.maestro.shell import CommandBuildTarget

SCRIPT_DIR    = os.path.dirname(__file__)
SRC_DIR       = os.path.join(SCRIPT_DIR, 'src')
OPENAPI_3_DIR = os.path.join(SCRIPT_DIR, 'v3.0')
OPENAPI_2_DIR = os.path.join(SCRIPT_DIR, 'v2.0')

class ConvertAPIBuildTarget(SingleBuildTarget):
    def __init__(self,file_from,spec_from,file_to,spec_to, fileformat='yaml', api_spec_converter_binary=None, dependencies=[]):
        self.file_from=file_from
        self.spec_from=spec_from
        self.file_to=file_to
        self.spec_to=spec_to
        self.fileformat=fileformat

        CMD='.cmd' if os_utils.is_windows() else ''

        self.API_SPEC_CONVERTER=api_spec_converter_binary
        if self.API_SPEC_CONVERTER is None and os.path.isfile(os.path.join('node_modules', '.bin', 'api-spec-converter'+CMD)):
            self.API_SPEC_CONVERTER=os.path.join('node_modules', '.bin', 'api-spec-converter'+CMD)
        if self.API_SPEC_CONVERTER is None:
            self.API_SPEC_CONVERTER=os_utils.which('api-spec-converter'+CMD)

        super().__init__(file_to, files=[self.file_from], dependencies=dependencies)

    def build(self):
        os_utils.ensureDirExists(os.path.dirname(self.file_to))
        stdout, stderr = os_utils.cmd_output([self.API_SPEC_CONVERTER, '--from', self.spec_from, '--to', self.spec_to, '--format', self.fileformat, self.file_from], echo=self.should_echo_commands(), critical=True, globbify=False)
        with open(self.file_to, 'wb') as w:
            w.write(stdout+stderr)

bm = BuildMaestro()
bundle = bm.add(CommandBuildTarget(
    targets=[
        os.path.join(OPENAPI_3_DIR, '4chan.swagger.bundle.yml')
    ],
    files=[
        os.path.join(OPENAPI_3_DIR, '4chan.swagger.yml')
    ],
    cmd=['node_modules\.bin\swagger-cli.cmd', 'bundle', '-o', os.path.join(OPENAPI_3_DIR, '4chan.swagger.bundle.yml'), os.path.join(OPENAPI_3_DIR, '4chan.swagger.yml')],
    echo=False,
    show_output=False))

def convert3to2(unit_name):
    #print(repr(unit_name))
    openapi_2_file = os.path.join(*([OPENAPI_2_DIR]+list(unit_name)))
    openapi_3_file = os.path.join(*([OPENAPI_3_DIR]+list(unit_name)))
    threeToTwo = bm.add(ConvertAPIBuildTarget(openapi_3_file+'.yml', 'openapi_3', openapi_2_file+'.yml', 'openapi_2', 'yaml'))
    yaml2json = bm.add(ConvertDataBuildTarget(openapi_2_file+'.json', filename=openapi_3_file+'.yml', from_type=EDataType.YAML, to_type=EDataType.JSON, dependencies=[threeToTwo.target]))
    return yaml2json

bm.add(ConvertDataBuildTarget(os.path.join(OPENAPI_3_DIR, '4chan.swagger.json'),               filename=os.path.join(OPENAPI_3_DIR, '4chan.swagger.yml'), from_type=EDataType.YAML, to_type=EDataType.JSON))
bm.add(ConvertDataBuildTarget(os.path.join(OPENAPI_3_DIR, '4chan.swagger.bundle.json'),        filename=os.path.join(OPENAPI_3_DIR, '4chan.swagger.bundle.yml'), from_type=EDataType.YAML, to_type=EDataType.JSON))
bm.add(ConvertDataBuildTarget(os.path.join(OPENAPI_3_DIR, 'definitions', 'Board.json'),        filename=os.path.join(OPENAPI_3_DIR, 'definitions', 'Board.yml'), from_type=EDataType.YAML, to_type=EDataType.JSON))
bm.add(ConvertDataBuildTarget(os.path.join(OPENAPI_3_DIR, 'definitions', 'CatalogEntry.json'), filename=os.path.join(OPENAPI_3_DIR, 'definitions', 'CatalogEntry.yml'), from_type=EDataType.YAML, to_type=EDataType.JSON))
bm.add(ConvertDataBuildTarget(os.path.join(OPENAPI_3_DIR, 'definitions', 'Post.json'),         filename=os.path.join(OPENAPI_3_DIR, 'definitions', 'Post.yml'), from_type=EDataType.YAML, to_type=EDataType.JSON))

convert3to2(['4chan.swagger'])
convert3to2(['4chan.swagger.bundle'])
convert3to2(['definitions', 'Board'])
convert3to2(['definitions', 'CatalogEntry'])
convert3to2(['definitions', 'Post'])
bm.as_app()
