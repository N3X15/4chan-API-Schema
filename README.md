# 4chan API Schema
This respository contains [JSON schemas](https://json-schema.org/), as well as two versions of an [OpenAPI](https://www.openapis.org/) schema that can validate 4chan's API, and, more importantly, describe its structure in detail.

The JSON schema files were initially based on 4chan's [official API documentation](https://github.com/4chan/4chan-API), but have been updated to conform with actual API data structures encountered.  They will also be used as the basis for for more extensive documentation.

# Why?

The 4chan API documentation is woefully out of date, as well as inaccurate.  In addition, data from the actual 4chan API can occasionally be received in a broken, yet well-formed state.  As someone developing an archival bot and search engine, the need for a way to verify the structure of API structures is paramount.

In addition, schemas such as these are also *descriptive*, meaning that they also can serve as documentation.  OpenAPI is very good in this regard, and is able to generate detailed documentation of RESTful APIs, including examples.

# Usage

<table><thead><th>Path</th><th>Type of Content</th></thead><tbody><tr><th>[`json_schema/`](json_schema)</td><td>[JSON Schema, Draft 4](https://json-schema.org/specification-links.html#draft-4)</td></tr><tr><td>[`OpenAPI/v2.0/`](OpenAPI/v2.0/)</td><td>[OpenAPI (fka Swagger) v2.0](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md)</td></tr><tr><td>[`OpenAPI/v3.0/`](OpenAPI/v3.0/)</td><td>[OpenAPI v3.0](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md)</td></tr></tbody></table>

Simply validate your JSON against the appropriate schema to check its structure.

**NOTE:** Although JSON Schema and OpenAPI 3 are a bit out of date, bear in mind that JSON Schema draft-4 and OpenAPI 3.0.0 were the most widely-adopted when I wrote all this crap.  If you have a need for something using a more up to date schema, please create an issue.

## IMPORTANT
**The 4chan API has rate limits** that you must follow or you will be blocked from the site.  Refer to the official 4chan API documentation for current rate limits.

# License

These files are &copy;2015-2018 Rob "N3X15" Nelson and are licensed under the MIT Open-Source License.

See [LICENSE](LICENSE) for more information.

Enjoy.

# Support

Inaccuracies can be reported in the GitHub issues panel. Yell at me in [#4chan](irc://irc.rizon.net/4chan) if I'm slow.

All other support will not be available.
